const API_URL = 'https://api.github.com/';

const rootElement = document.getElementById('root');

const loadingElement = document.getElementById('loading-overlay');

const appStart = function() {
    const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
    const fighterPromise = callApi(enpoint);

    fighterPromise.then(fighters => {
        const fightersNames = getFightersNames(fighters);
        rootElement.innerText = fighterNames;
    });
};

function callApi(endpoint, method = 'GET') {
    const url = API_URL . endpoint;
    const options = [
        method
    ];

    return fetch(API_URL)
        .then(response => response.ok ? response.json() : Promise.reject(new Error("Failed load data")))
        .then(file => JSON.parse(atob(file.content)))
        .catch(error => {
            console.warn(error);
            rootElement.innerText = "Failed to load data";
        })
        .finally(() => {
            loadingElement.remove();
        });
}

function getFightersNames(fighters) {
    const names = fighters.map(fighter => fighter.name).join('\n');

    return names;
}

appStart();
